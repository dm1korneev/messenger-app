# messenger-app

## Build

1. Install [Node.js](https://nodejs.org);
2. Run `npm install` to install dependencies;
3. Run `npm run build`. It compiles app into `/dist`.

## Develop

Run `npm start` to start development server with hot loading on `http://localhost:3000`.
